#ifndef BONDALGOSERVICE_HPP
#define BONDALGOSERVICE_HPP

#include "executionservice.hpp"
template<typename T>
class AlgoExecution
{
private:
	ExecutionOrder<T>& d_executionOrder;
	OrderBook<T>& d_orderbook;
public:
	//constructor and destructor
	AlgoExecution(ExecutionOrder<T> _executionOrder,
				  OrderBook<T> _orderbook)
				  :d_executionOrder(_executionOrder),
				   d_orderbook(_orderbook)
				  {}
	~AlgoExecution() {};

	//get Execution order
	ExecutionOrder<T>& GetExecutionOrder() const {
		return d_executionOrder;
	}
	OrderBook<T>& GetOrderBook() const {
		return d_orderbook;
	}

};


template<typename T>
class AlgoExecutionService : public Service<string, AlgoExecution<T>>
{
};

class BondAlgoExecutionService : public Service< string, OrderBook<Bond> >
{
public:
	//ctor
	BondAlgoExecutionService(BondExecutionService &_executionService) : d_service(_executionService),orderCount(0)
	{}

	OrderBook<Bond>& GetData(string key) { exit(-1); }

	void OnMessage(OrderBook<Bond> &orderbook);

	void AddListener(ServiceListener< OrderBook<Bond> >* listener);

	const vector< ServiceListener< OrderBook<Bond> >* >& GetListeners() const;

private:
	
	map<string, PricingSide> sidesMapping;
	vector< ServiceListener< OrderBook<Bond> >* > listenersCollection;
	int orderCount;
	BondExecutionService& d_service;
};


//aggress the top of the book
void BondAlgoExecutionService::OnMessage(OrderBook<Bond>& orderbook)
{
	string cusip = orderbook.GetProduct().GetProductId();

	auto iter = sidesMapping.find(cusip);
	

	if (iter != sidesMapping.end())
	{
		
		PricingSide side = iter->second;
		string orderId = "TR" + to_string(orderCount+1);
		double price = iter->second == BID ? orderbook.GetOfferStack()[0].GetPrice() : orderbook.GetBidStack()[0].GetPrice();
		double quantity = (double)(iter->second == BID ? orderbook.GetOfferStack()[0].GetQuantity() : orderbook.GetBidStack()[0].GetQuantity());
		string parentorderId = "N/A";
		ExecutionOrder<Bond> new_order(Bond(cusip), side, orderId, MARKET, price, quantity, 0, parentorderId, false);
		d_service.ExecuteOrder(new_order);
		if (iter->second == BID)
			iter->second = OFFER;
		else iter->second = BID;
		
		orderCount++;
	}
	else
	{
		PricingSide side = BID;
		string orderId = "T" + to_string(orderCount+1);
		double price = orderbook.GetOfferStack()[0].GetPrice();
		double quantity = (double)orderbook.GetOfferStack()[0].GetQuantity();
		string parentorderId = "N/A";
		ExecutionOrder<Bond> new_order(Bond(cusip), side, orderId, MARKET, price, quantity, 0, parentorderId, false);
		d_service.ExecuteOrder(new_order);
		sidesMapping[cusip] = OFFER;

		orderCount++;
	}
}

const vector<ServiceListener<OrderBook<Bond>>*>& BondAlgoExecutionService::GetListeners() const
{
	return listenersCollection;
}

void BondAlgoExecutionService::AddListener(ServiceListener<OrderBook<Bond>>* listener)
{
	listenersCollection.push_back(listener);
}



#endif