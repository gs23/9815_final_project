#ifndef BONDALGOSTREAMING_HPP
#define BONDALGOSTREAMING_HPP


#include "streamingservice.hpp"
#include "pricingservice.hpp"


using namespace std;

template<typename T>
class AlgoStream
{
private:
	PriceStream<T>& d_priceStream ;
public:
	//default constructor
	AlgoStream() {}
	AlgoStream(const T &_product, const PriceStreamOrder &_bidOrder, const PriceStreamOrder &_offerOrder) :d_priceStream(_product, _bidOrder, _offerOrder) {}

	//get the priceStream
	PriceStream<T>& GetStreamingOrder() const
	{
		return d_priceStream;
	}

};

template<typename T>
class AlgoStreamingService : public Service<string, AlgoStream<T>>
{

};

class BondAlgoStreamingService : public Service< string, Price<Bond> >
{
public:
	BondAlgoStreamingService(BondStreamingService &_streaming_service) : d_service(_streaming_service) {}

	Price<Bond>& GetData(string cusip) { exit(-1); }

	void OnMessage(Price<Bond> &algoStream);

	void AddListener(ServiceListener< Price<Bond> >* listener);

	const vector< ServiceListener< Price<Bond> >* >& GetListeners() const;

private:
	BondStreamingService & d_service;
	vector< ServiceListener< Price<Bond> >* > listenersCollection;

};



void BondAlgoStreamingService::OnMessage(Price<Bond>& price)
{
	PriceStreamOrder bid_order(price.GetMid() - price.GetBidOfferSpread() / 2.0, 10000000, 0, BID), offer_order(price.GetMid() + price.GetBidOfferSpread() / 2.0, 10000000, 0, OFFER);
	PriceStream<Bond> priceStream(price.GetProduct(), bid_order, offer_order);
	d_service.PublishPrice(priceStream);
}


void BondAlgoStreamingService::AddListener(ServiceListener<Price<Bond>>* listener)
{
	listenersCollection.push_back(listener);
	
}


const vector<ServiceListener<Price<Bond>>*>& BondAlgoStreamingService::GetListeners() const
{
	return listenersCollection;
}


#endif