EXEC += test


CXX := g++
CXXFLAGS += -std=c++11  -W  -g	
#-Wall -Wextra
# Default rule for creating a .o file from a .cpp file

	
all: $(EXEC)

# Default rule for creating an exec of $(EXEC) from a .o file
$(EXEC): test.o 
	$(CXX) $(CXXFLAGS)  -o $@ $^ $(TAIL)
	
%.o: %.cpp 
	$(CXX) $(CXXFLAGS)  -c -o $@ $< 

clean:
	-$(RM) *.o $(EXEC)