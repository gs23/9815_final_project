##Content
1. *.py files are for data simulation.
2.  *.hpp files are implementation for the trading system.
3.  test.cpp file is a simple program to test the system.
4. *.txt files contain the simulated data.
5.  the system will write output to the /output directory.
6. Makefile is the makefile to compile the system.

## Usage
use make to compile the code:
```shell
> make
```
This will give you executable named test. Execute it to get the output in  the output directory。
```shell
>./test
```