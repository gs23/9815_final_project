/**
 * executionservice.hpp
 * Defines the data types and Service for executions.
 *
 * @author Breman Thuraisingham
 */
#ifndef EXECUTION_SERVICE_HPP
#define EXECUTION_SERVICE_HPP

#include <string>
#include "soa.hpp"
#include "marketdataservice.hpp"
#include <map>
#include <fstream>

enum OrderType { FOK, IOC, MARKET, LIMIT, STOP };

enum Market { BROKERTEC, ESPEED, CME };

/**
 * An execution order that can be placed on an exchange.
 * Type T is the product type.
 */
template<typename T>
class ExecutionOrder
{

public:

  // ctor for an order
  ExecutionOrder(const T &_product, PricingSide _side, string _orderId, OrderType _orderType, double _price, double _visibleQuantity, double _hiddenQuantity, string _parentOrderId, bool _isChildOrder);
  ExecutionOrder(const T _product) :product{ _product } {}
  // Get the product
  const T& GetProduct() const;

  // Get the order ID
  const string& GetOrderId() const;

  // Get the order type on this order
  OrderType GetOrderType() const;

  // Get the price on this order
  double GetPrice() const;

  // Get the side of this order
  PricingSide GetSide() const;

  // Get the visible quantity on this order
  long GetVisibleQuantity() const;

  // Get the hidden quantity
  long GetHiddenQuantity() const;

  // Get the parent order ID
  const string& GetParentOrderId() const;

  // Is child order?
  bool IsChildOrder() const;

private:
  T product;
  PricingSide side;
  string orderId;
  OrderType orderType;
  double price;
  double visibleQuantity;
  double hiddenQuantity;
  string parentOrderId;
  bool isChildOrder;

};

/**
 * Service for executing orders on an exchange.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class ExecutionService : public Service<string,ExecutionOrder <T> >
{

public:

  // Execute an order on a market
  virtual void ExecuteOrder(ExecutionOrder<T>& order) = 0;

};


class BondExecutionService : public ExecutionService<Bond>
{
public:
	ExecutionOrder<Bond>& GetData(string orderId);

	void OnMessage(ExecutionOrder<Bond> &order);

	void AddListener(ServiceListener< ExecutionOrder<Bond> >* listener);

	const vector< ServiceListener< ExecutionOrder<Bond> >* >& GetListeners() const;

	void ExecuteOrder(ExecutionOrder<Bond>& order);

private:
	map<string, ExecutionOrder<Bond> > orderMapping;
	vector< ServiceListener< ExecutionOrder<Bond> >* > listenersCollection;

};


class BondExecutionConnector : Connector<Bond>
{
public:
	BondExecutionConnector(BondExecutionService& targetService) : d_service(targetService) {}

	void Publish(Bond& data);
private:
	BondExecutionService& d_service;

};
//*********************************************************************************************
// implmentation part for ExecutionOrder class:

template<typename T>
ExecutionOrder<T>::ExecutionOrder(const T &_product, PricingSide _side, string _orderId, OrderType _orderType, double _price, double _visibleQuantity, double _hiddenQuantity, string _parentOrderId, bool _isChildOrder) :
  product(_product)
{
  side = _side;
  orderId = _orderId;
  orderType = _orderType;
  price = _price;
  visibleQuantity = _visibleQuantity;
  hiddenQuantity = _hiddenQuantity;
  parentOrderId = _parentOrderId;
  isChildOrder = _isChildOrder;
}

template<typename T>
const T& ExecutionOrder<T>::GetProduct() const
{
  return product;
}

template<typename T>
const string& ExecutionOrder<T>::GetOrderId() const
{
  return orderId;
}

template<typename T>
OrderType ExecutionOrder<T>::GetOrderType() const
{
  return orderType;
}

template<typename T>
double ExecutionOrder<T>::GetPrice() const
{
  return price;
}

template<typename T>
inline PricingSide ExecutionOrder<T>::GetSide() const
{
	return side;
}

template<typename T>
long ExecutionOrder<T>::GetVisibleQuantity() const
{
  return visibleQuantity;
}

template<typename T>
long ExecutionOrder<T>::GetHiddenQuantity() const
{
  return hiddenQuantity;
}

template<typename T>
const string& ExecutionOrder<T>::GetParentOrderId() const
{
  return parentOrderId;
}

template<typename T>
bool ExecutionOrder<T>::IsChildOrder() const
{
  return isChildOrder;
}


//*********************************************************************************************

//*********************************************************************************************
// implmentation part for BondExecutionService class:

ExecutionOrder<Bond>& BondExecutionService::GetData(string orderId)
{
	auto iter = orderMapping.find(orderId);
	if (iter != orderMapping.end())
	{
		return iter->second;
	}
	else
	{
		cout << "Order Not Found." << endl;
		exit(-1);
	}
}

void BondExecutionService::OnMessage(ExecutionOrder<Bond>& order)
{
	for (auto listener : listenersCollection)
	{
		listener->ProcessAdd(order);
	}

}

void BondExecutionService::AddListener(ServiceListener<ExecutionOrder<Bond>>* listener)
{
	listenersCollection.push_back(listener);
}


const vector<ServiceListener<ExecutionOrder<Bond>>*>& BondExecutionService::GetListeners() const
{
	return listenersCollection;
}


void BondExecutionService::ExecuteOrder(ExecutionOrder<Bond>& order)
{
	orderMapping.insert(make_pair(order.GetOrderId(), order));
	for (auto listener : listenersCollection)
		listener->ProcessAdd(order);
}


//*********************************************************************************************
//*********************************************************************************************
// implmentation part for BondExecutionConnector class:


void BondExecutionConnector::Publish(Bond & data)
{
}
#endif
