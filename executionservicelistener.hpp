#ifndef EXECUTIONLISTENER_HPP
#define EXECUTIONLISTENER_HPP

#include "historicaldataservice.hpp"

using namespace std;

class ExecutionServiceListener : public ServiceListener< ExecutionOrder<Bond> >
{
public:
	//ctor
	ExecutionServiceListener(BondHistoricalExecutionDataService &_historicalExecutionService) : d_service(_historicalExecutionService) {}

	void ProcessAdd(ExecutionOrder<Bond> &order);

	void ProcessRemove(ExecutionOrder<Bond> &order) {}

	void ProcessUpdate(ExecutionOrder<Bond> &order) {}

private:
	BondHistoricalExecutionDataService &d_service;

};

void ExecutionServiceListener::ProcessAdd(ExecutionOrder<Bond> &order)
{
	d_service.OnMessage(order);
}
#endif