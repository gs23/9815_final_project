/**
 * historicaldataservice.hpp
 * historicaldataservice.hpp
 *
 * @author Breman Thuraisingham
 * Defines the data types and Service for historical data.
 *
 * @author Breman Thuraisingham
 */
#ifndef HISTORICAL_DATA_SERVICE_HPP
#define HISTORICAL_DATA_SERVICE_HPP
#include <vector>
#include <string>
#include "executionservice.hpp"
//#include "riskservice.hpp"
#include "streamingservice.hpp"
#include "inquiryservice.hpp"
#include "positionservice.hpp"
/**
 * Service for processing and persisting historical data to a persistent store.
 * Keyed on some persistent key.
 * Type T is the data type to persist.
 */

string BondPriceOutputConverter(double price)
{
	double decimal_int_part, decimal_part;
	decimal_part = modf(price, &decimal_int_part);
	string int_part = to_string((int)decimal_int_part);
	decimal_part *= 32;
	int fract_part1, fract_part2;
	fract_part1 = (int)floor(decimal_part);
	fract_part2 = (int)((decimal_part - fract_part1) * 8);
	string last_digit, result(int_part);
	result += "-";
	if (fract_part1 < 10) result += "0";
	result += to_string(fract_part1);
	last_digit = fract_part2 == 4 ? "+" : to_string(fract_part2);
	result += last_digit;
	return result;
}

string BidOfferString(PricingSide side)
{
	return side == BID ?  "BID" : "OFFER";
}
string OrderTypeString(OrderType order)
{
	switch (order)
	{
	case FOK:
		return "FOK";
		break;
	case IOC:
		return "IOC";
		break;
	case MARKET:
		return "MARKET";
		break;
	case LIMIT:
		return "LIMIT";
		break;
	case STOP:
		return "STOP";
		break;
	}
}
string BuySell(Side side)
{
	return side == BUY ? "BUY" : "SELL";
}

string InquiryStateString(InquiryState state)
{
	switch (state)
	{
	case RECEIVED:
		return "RECEIVED";
		break;
	case QUOTED:
		return "QUOTED";
		break;
	case DONE:
		return "DONE";
		break;
	case REJECTED:
		return "REJECTED";
		break;
	case CUSTOMER_REJECTED:
		return "CUSTOMER_REJECTED";
		break;
	}
}

template<typename T>
class HistoricalDataService : Service<string,T>
{

public:

  // Persist data to a store
  virtual void PersistData(string persistKey, T& data) = 0;

};

class BondHistoricalPositionDataConnector : public Connector< Position<Bond> >
{
public:
	void Publish(Position<Bond>& data);
};


class BondHistoricalPositionDataService : public HistoricalDataService< Position<Bond> >
{
public:
	BondHistoricalPositionDataService() : num(1) {}
	
	BondHistoricalPositionDataService(BondHistoricalPositionDataConnector _connector) : num(1),d_connector(_connector){}
	
	Position<Bond>& GetData(string key) { exit(-1); }

	void OnMessage(Position<Bond>& data);

	void AddListener(ServiceListener< Position<Bond> >* listener) {}

	const vector< ServiceListener< Position<Bond> >*>& GetListeners() const
	{
		exit(-1);
	}

	void PersistData(string persistKey, Position<Bond>& data);
private:
	int num;
	BondHistoricalPositionDataConnector d_connector;
};


class BondHistoricalExecutionDataConnector : public Connector< ExecutionOrder<Bond> >
{
public:
	void Publish(ExecutionOrder<Bond>& data);
};


class BondHistoricalExecutionDataService : public HistoricalDataService< ExecutionOrder<Bond> >
{
public:

	BondHistoricalExecutionDataService() : num(1) {}

	BondHistoricalExecutionDataService(BondHistoricalExecutionDataConnector _connector) : num(1), d_connector(_connector) {}

	ExecutionOrder<Bond>& GetData(string key) { exit(-1); }

	void OnMessage(ExecutionOrder<Bond>& data);

	void AddListener(ServiceListener< ExecutionOrder<Bond> >* listener) {}

	const vector< ServiceListener< ExecutionOrder<Bond> >*>& GetListeners() const
	{
		exit(-1);
	}

	void PersistData(string persistKey, ExecutionOrder<Bond>& data);
private:
	int num;
	BondHistoricalExecutionDataConnector d_connector;
};


class BondHistoricalRiskDataConnector : public Connector< map<string, PV01<Bond> > >
{
public:
	void Publish(map<string, PV01<Bond> >& data);
};


class BondHistoricalRiskDataService : public HistoricalDataService<map<string, PV01<Bond> > >
{
public:
	BondHistoricalRiskDataService() : num(1) {}

	BondHistoricalRiskDataService(BondHistoricalRiskDataConnector _connector) : num(1), d_connector(_connector) {}

	map<string, PV01<Bond> >& GetData(string key) { exit(-1); }

	void OnMessage(map<string, PV01<Bond>> & data);

	void AddListener(ServiceListener< map<string, PV01<Bond> > >* listener) {}

	const vector< ServiceListener< map<string, PV01<Bond> > >*>& GetListeners() const
	{
		exit(-1);
	}

	void PersistData(string persistKey, map<string, PV01<Bond>>& data);
private:
	int num;
	BondHistoricalRiskDataConnector d_connector;
};



class BondHistoricalStreamingDataConnector : public Connector< PriceStream<Bond> >
{
public:
	void Publish(PriceStream<Bond>& data);
};


class BondHistoricalStreamingDataService : public HistoricalDataService< PriceStream<Bond> >
{
public:
	BondHistoricalStreamingDataService() : num(1) {}

	BondHistoricalStreamingDataService(BondHistoricalStreamingDataConnector _connector) : num(1), d_connector(_connector) {}

	PriceStream<Bond>& GetData(string key) { exit(-1); }

	void OnMessage(PriceStream<Bond>& data);

	void AddListener(ServiceListener< PriceStream<Bond> >* listener) {}

	const vector< ServiceListener< PriceStream<Bond> >*>& GetListeners() const
	{exit(-1);}

	void PersistData(string persistKey, PriceStream<Bond>& data);
private:
	int num;
	BondHistoricalStreamingDataConnector d_connector;
};


class BondHistoricalInquiryDataConnector : public Connector< Inquiry<Bond> >
{
public:
	void Publish(Inquiry<Bond>& data);
};

class BondHistoricalInquiryDataService : public HistoricalDataService< Inquiry<Bond> >
{
public:
	BondHistoricalInquiryDataService() : num(1) {}

	BondHistoricalInquiryDataService(BondHistoricalInquiryDataConnector _connector) : num(1), d_connector(_connector) {}

	Inquiry<Bond>& GetData(string key) { exit(-1); }

	void OnMessage(Inquiry<Bond>& data);

	void AddListener(ServiceListener< Inquiry<Bond> >* listener) {}

	const vector< ServiceListener< Inquiry<Bond> >*>& GetListeners() const { exit(-1); }

	void PersistData(string persistKey, Inquiry<Bond>& data);
private:
	int num;
	BondHistoricalInquiryDataConnector d_connector;
};

void BondHistoricalPositionDataService::OnMessage(Position<Bond>& data)
{
	PersistData(to_string(num), data);
}

void BondHistoricalPositionDataService::PersistData(string persistKey, Position<Bond>& data)
{
	fstream output("output/position_output.txt", ios::out|ios::app);
	if (num == 1)
		output << setw(5) << "Key" << setw(13) << "productID" << setw(10) << "Coupon" << setw(15) << "Maturity Date" << setw(20) << "Aggregate Position" << setw(10) << "TRSY1" << setw(10) << "TRSY2" << setw(10) << "TRSY3" << endl;
	
	output << setw(5) << num;
	num++;
	d_connector.Publish(data);
	output.close();
}

void BondHistoricalPositionDataConnector::Publish(Position<Bond>& data)
{
	fstream output("output/position_output.txt", ios::out|ios::app);
	vector<string> book = { "TRSY1", "TRSY2", "TRSY3" };
	output << setw(13) << data.GetProduct().GetProductId() << setw(10) << data.GetProduct().GetCoupon() << "    " << data.GetProduct().GetMaturityDate() << setw(20) << data.GetAggregatePosition() << setw(10) << data.GetPosition(book[0]) << setw(10) << data.GetPosition(book[1]) << setw(10) << data.GetPosition(book[2]) << endl;
	output.close();
	
}

void BondHistoricalExecutionDataService::OnMessage(ExecutionOrder<Bond>& data)
{
	PersistData(to_string(num), data);
}

void BondHistoricalExecutionDataService::PersistData(string persistKey, ExecutionOrder<Bond>& data)
{
	fstream output("output/execution_output.txt", ios::out|ios::app);
	if (num == 1)
		output << setw(5) << "Key" << setw(15) << "ProductID" << setw(10) << "Side" << setw(10) << "OrderID" << setw(13) << "OrderType" << setw(10) << "Price" << setw(18) << "VisibleQuantity" << setw(18) << "HiddenQuantity" << setw(18) << "ParentOrderID" << setw(15) << "IsChildOrder" << endl;

	output << setw(5) << num;
	
	num++;
	output.close();
	d_connector.Publish(data);
}

void BondHistoricalExecutionDataConnector::Publish(ExecutionOrder<Bond>& data)
{
	fstream output("output/execution_output.txt", ios::out|ios::app);
	output << setw(15) << data.GetProduct().GetProductId() << setw(10) << BidOfferString(data.GetSide()) << setw(10) << data.GetOrderId() << setw(13) << OrderTypeString(data.GetOrderType()) << setw(10) << BondPriceOutputConverter(data.GetPrice()) << setw(18) << data.GetVisibleQuantity() << setw(18) << data.GetHiddenQuantity() << setw(18) << data.GetParentOrderId() << setw(15) << "FALSE" << endl;
	output.close();
}


void BondHistoricalRiskDataConnector::Publish(map<string, PV01<Bond>>& data)
{
	fstream output("output/risk_output.txt", ios::out |ios::app);
	output << "      ";
	for (auto pair : data)
	{
		
		output<<fixed<< pair.second.GetProducts()[0].GetProductId() << "    " << pair.second.GetProducts()[0].GetCoupon() << "    " << pair.second.GetProducts()[0].GetMaturityDate() << "    " << pair.second.GetQuantity() * pair.second.GetPV01() << endl;
	}
	output.close();
}


void BondHistoricalRiskDataService::OnMessage(map<string, PV01<Bond>>& data)
{
	PersistData(to_string(num), data);
}


void BondHistoricalRiskDataService::PersistData(string persistKey, map<string, PV01<Bond>>& data)
{
	fstream output("output/risk_output.txt", ios::out | ios::app);
	if (num == 1)
		output << setw(5) << "    " << "ProductID    Coupon      Maturity Date  Total Risk" << endl;

	output << setw(5) << num;

	num++;
	output.close();
	d_connector.Publish(data);
}

void BondHistoricalStreamingDataService::OnMessage(PriceStream<Bond>& data)
{
	PersistData(to_string(num), data);
}

void BondHistoricalStreamingDataService::PersistData(string persistKey, PriceStream<Bond>& data)
{
	fstream output("output/stream_output.txt", ios::out|ios::app);
	if (num == 1)
		output << setw(5) << "Key" << setw(15) << "ProductID" << setw(15) << "Bid Price" << setw(15) << "Quantity" << setw(15) << "Offer Price" << setw(15) << "Quantity" << endl;

	output << setw(5) << num;

	num++;
	output.close();
	d_connector.Publish(data);
}

void BondHistoricalStreamingDataConnector::Publish(PriceStream<Bond>& data)
{
	fstream output("output/stream_output.txt", ios::out|ios::app);
	output << setw(15) << data.GetProduct().GetProductId() << setw(15) << BondPriceOutputConverter(data.GetBidOrder().GetPrice()) << setw(15) << data.GetBidOrder().GetVisibleQuantity() << setw(15) << BondPriceOutputConverter(data.GetOfferOrder().GetPrice()) << setw(15) << data.GetOfferOrder().GetVisibleQuantity() << endl;
	output.close();
}

void BondHistoricalInquiryDataService::OnMessage(Inquiry<Bond>& data)
{
	PersistData(to_string(num), data);
}

void BondHistoricalInquiryDataService::PersistData(string persistKey, Inquiry<Bond>& data)
{
	fstream output("output/inquiry_output.txt", ios::out|ios::app);
	if (num == 1)
		output << setw(5) << "Key" << setw(13) << "InquiryID" << setw(15) << "ProductID" << setw(10) << "Side" << setw(15) << "Quantity" << setw(15) << "Price" << setw(10) << "State" << endl;

	output << setw(5) << num;

	num++;
	output.close();
	d_connector.Publish(data);

}



void BondHistoricalInquiryDataConnector::Publish(Inquiry<Bond>& data)
{
	fstream output("output/inquiry_output.txt", ios::out|ios::app);
	output << setw(13) << data.GetInquiryId() << setw(15) << data.GetProduct().GetProductId() << setw(10) << BuySell(data.GetSide()) << setw(15) << data.GetQuantity() << setw(15) << BondPriceOutputConverter(data.GetPrice()) << setw(10) << InquiryStateString(data.GetState()) << endl;
	output.close();
}
#endif
