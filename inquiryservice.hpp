/**
 * inquiryservice.hpp
 * Defines the data types and Service for customer inquiries.
 *
 * @author Breman Thuraisingham
 */
#ifndef INQUIRY_SERVICE_HPP
#define INQUIRY_SERVICE_HPP

#include "soa.hpp"
#include "tradebookingservice.hpp"
#include <map>

// Various inqyury states
enum InquiryState { RECEIVED, QUOTED, DONE, REJECTED, CUSTOMER_REJECTED };

/**
 * Inquiry object modeling a customer inquiry from a client.
 * Type T is the product type.
 */
template<typename T>
class Inquiry
{

public:

  // ctor for an inquiry
  Inquiry(string _inquiryId, const T &_product, Side _side, long _quantity, double _price, InquiryState _state);

  Inquiry() {}
  // Get the inquiry ID
  const string& GetInquiryId() const;

  // Get the product
  const T& GetProduct() const;

  // Get the side on the inquiry
  Side GetSide() const;

  // Get the quantity that the client is inquiring for
  long GetQuantity() const;

  // Get the price that we have responded back with
  double GetPrice() const;

  // Get the current state on the inquiry
  InquiryState GetState() const;

  void SetState(InquiryState _state);

private:
  string inquiryId;
  T product;
  Side side;
  long quantity;
  double price;
  InquiryState state;

};

/**
 * Service for customer inquirry objects.
 * Keyed on inquiry identifier (NOTE: this is NOT a product identifier since each inquiry must be unique).
 * Type T is the product type.
 */
template<typename T>
class InquiryService : public Service<string,Inquiry <T> >
{

public:

  // Send a quote back to the client
  virtual void SendQuote(const string &inquiryId, double price) = 0;

  // Reject an inquiry from the client
  virtual void RejectInquiry(const string &inquiryId)= 0;

};

class BondInquiryService : public InquiryService<Bond>
{
public:
	Inquiry<Bond>& GetData(string inquiryId);

	void OnMessage(Inquiry<Bond> &inquiry);

	void AddListener(ServiceListener< Inquiry<Bond> >* listener);

	const vector< ServiceListener< Inquiry<Bond> >* >& GetListeners() const;
	
	void SendQuote(const string &inquiryId, double price);
	
	void RejectInquiry(const string &inquiryId);

private:
	vector< ServiceListener< Inquiry<Bond> >* > listenersCollection;
	map<string, Inquiry<Bond>> inquiryMapping;
};

class BondInquiryConnector : public Connector< Inquiry<Bond> >
{
public:

	//ctor
	BondInquiryConnector(BondInquiryService& targetService) : d_service(targetService) {
	};

	//subscirbe method to flow the data into service
	void ReadFromSource(string SourceName);

	void Publish(Inquiry<Bond> &data);

private:
	BondInquiryService& d_service;

};

//*********************************************************************************************
// implmentation part for Inquiry class:

template<typename T>
Inquiry<T>::Inquiry(string _inquiryId, const T &_product, Side _side, long _quantity, double _price, InquiryState _state) :
  product(_product)
{
  inquiryId = _inquiryId;
  side = _side;
  quantity = _quantity;
  price = _price;
  state = _state;
}

template<typename T>
const string& Inquiry<T>::GetInquiryId() const
{
  return inquiryId;
}

template<typename T>
const T& Inquiry<T>::GetProduct() const
{
  return product;
}

template<typename T>
Side Inquiry<T>::GetSide() const
{
  return side;
}

template<typename T>
long Inquiry<T>::GetQuantity() const
{
  return quantity;
}

template<typename T>
double Inquiry<T>::GetPrice() const
{
  return price;
}

template<typename T>
InquiryState Inquiry<T>::GetState() const
{
  return state;
}


template<typename T>
void Inquiry<T>::SetState(InquiryState _state)
{
	state = _state;
}
//*********************************************************************************************
// implmentation part for BondInquiryService class:

Inquiry<Bond>& BondInquiryService::GetData(string inquiryId)
{
	auto iter = inquiryMapping.find(inquiryId);
	if (iter != inquiryMapping.end())
	{
		return iter->second;
	}
	else
	{
		cout << "Inquiry not found. " << endl;
		exit(-1);
	}
}

void BondInquiryService::OnMessage(Inquiry<Bond>& inquiry)
{
	if (inquiry.GetState() == QUOTED)
	{
		auto iter = inquiryMapping.find(inquiry.GetInquiryId());
		
		iter->second.SetState(DONE);
		for (auto listener : listenersCollection)
			listener->ProcessAdd(iter->second);
	}
	if (inquiry.GetState() == RECEIVED)
	{
		Inquiry<Bond> newInquiry(inquiry.GetInquiryId(), inquiry.GetProduct(), inquiry.GetSide(), inquiry.GetQuantity(), 100, inquiry.GetState());
		inquiryMapping.insert(make_pair(inquiry.GetInquiryId(), newInquiry)) ;
	}

	
}

void BondInquiryService::AddListener(ServiceListener<Inquiry<Bond>>* listener)
{
	listenersCollection.push_back(listener);
}

const vector<ServiceListener<Inquiry<Bond>>*>& BondInquiryService::GetListeners() const
{
	return listenersCollection;
}

void BondInquiryService::SendQuote(const string & inquiryId, double price)
{
	
}

inline void BondInquiryService::RejectInquiry(const string & inquiryId)
{
}
//*********************************************************************************************



//*********************************************************************************************
// implmentation part for BondInquiryConnector class:

void BondInquiryConnector::ReadFromSource(string SourceName)
{

	fstream source(SourceName);
	string delimiter = ",";

	int pos = 0;
	string cur_line;
	getline(source, cur_line);
	while (source)
	{

		int pos = 0;

		//parse the inquiry id
		pos = cur_line.find(delimiter);
		string inquiryId = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		//parse the cusip number
		pos = cur_line.find(delimiter);
		string cusip= cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		//parse the trade side
		pos = cur_line.find(delimiter);
		string side = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		//parse the price
		pos = cur_line.find(delimiter);
		string price = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());
		
		//parse the quantity
		pos = cur_line.find(delimiter);
		string quantity = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());
		
		Inquiry<Bond> newInquiry(inquiryId, Bond(cusip), side == "B" ? BUY : SELL, stol(quantity), stod(price), RECEIVED);

		d_service.OnMessage(newInquiry);

		if (d_service.GetData(inquiryId).GetState() == RECEIVED)
		{
			Publish(d_service.GetData(inquiryId));
		}
	

		getline(source, cur_line);


	}

	source.close();
}

void BondInquiryConnector::Publish(Inquiry<Bond>& inquiry)
{
	Inquiry<Bond> tempInquiry(inquiry.GetInquiryId(), inquiry.GetProduct(), inquiry.GetSide(), inquiry.GetQuantity(), inquiry.GetPrice(), QUOTED);
	d_service.OnMessage(tempInquiry);
}


#endif
