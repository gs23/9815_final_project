#ifndef INQUIRYLISTENER_HPP
#define INQUIRYLISTENER_HPP
#include "historicaldataservice.hpp"

using namespace std;

class InquiryServiceListener : public ServiceListener< Inquiry<Bond> >
{
public:
	//ctor
	InquiryServiceListener(BondHistoricalInquiryDataService &_historicalInquiryService) : d_service(_historicalInquiryService) {}

	void ProcessAdd(Inquiry<Bond> &inquiry);

	void ProcessRemove(Inquiry<Bond> &inquiry) {}

	void ProcessUpdate(Inquiry<Bond> &inquiry) {}

private:
	BondHistoricalInquiryDataService &d_service;

};

void InquiryServiceListener::ProcessAdd(Inquiry<Bond> &inquiry)
{
	d_service.OnMessage(inquiry);
}

#endif