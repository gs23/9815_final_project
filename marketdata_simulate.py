import random
cusip_num = ["912828M12","912828N68","912828M59","912828R37","912828M46","912828R43"]

spread_list = ["+","2"]
last_digit = range(8)
last_digit = map(str, last_digit)
last_digit[4] = "+"

with open("marketdata.txt","w") as f:
    for cusip_number in cusip_num:
        for i in range(1000):          
            mid_price = str(random.randint(99,101))+"-"+\
                        str(random.randint(0,31)).zfill(2)+last_digit[random.randint(0,7)]
            record = ",".join([cusip_number,mid_price])
            f.write(record+"\n")