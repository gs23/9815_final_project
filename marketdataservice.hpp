/**
 * marketdataservice.hpp
 * Defines the data types and Service for order book market data.
 *
 * @author Breman Thuraisingham
 */
#ifndef MARKET_DATA_SERVICE_HPP
#define MARKET_DATA_SERVICE_HPP

#include <string>
#include <vector>
#include <map>
#include <fstream>
#include "soa.hpp"
#include "products.hpp"

using namespace std;

// Side for market data
enum PricingSide { BID, OFFER };

/**
 * A market data order with price, quantity, and side.
 */
class Order
{

public:

  // ctor for an order
  Order(double _price, long _quantity, PricingSide _side);

  // Get the price on the order
  double GetPrice() const;

  // Get the quantity on the order
  long GetQuantity() const;

  // Get the side on the order
  PricingSide GetSide() const;

private:
  double price;
  long quantity;
  PricingSide side;

};

/**
 * Class representing a bid and offer order
 */
class BidOffer
{

public:

  // ctor for bid/offer
  BidOffer(const Order &_bidOrder, const Order &_offerOrder);

  // Get the bid order
  const Order& GetBidOrder() const;

  // Get the offer order
  const Order& GetOfferOrder() const;

private:
  Order bidOrder;
  Order offerOrder;

};

/**
 * Order book with a bid and offer stack.
 * Type T is the product type.
 */
template<typename T>
class OrderBook
{

public:

  // ctor for the order book
  OrderBook(const T &_product, const vector<Order> &_bidStack, const vector<Order> &_offerStack);

  // Get the product
  const T& GetProduct() const;

  // Get the bid stack
  const vector<Order>& GetBidStack() const;

  // Get the offer stack
  const vector<Order>& GetOfferStack() const;

private:
  T product;
  vector<Order> bidStack;
  vector<Order> offerStack;

};

/**
 * Market Data Service which distributes market data
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class MarketDataService : public Service<string,OrderBook <T> >
{

public:

  // Get the best bid/offer order
  virtual const BidOffer& GetBestBidOffer(const string &productId) = 0;

  // Aggregate the order book
  //virtual const OrderBook<T>& AggregateDepth(const string &productId) = 0;

};


class BondMarketDataService : public MarketDataService<Bond>
{
public:
	// Get data on our service given a key
	OrderBook<Bond>& GetData(string cusip_num);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(OrderBook<Bond>& order_book);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener< OrderBook<Bond> >* listener) ;

	// Get all liysteners on the Service.
	const vector< ServiceListener< OrderBook<Bond> >* >& GetListeners() const;
	
	// Get the best bid/offer order
	const BidOffer& GetBestBidOffer(const string &cusip_num);
	
	// Aggregate the order book
	const OrderBook<Bond>& AggregateDepth(const string &cusip_num) {}
private:
	vector< OrderBook<Bond> > orderbooksCollection;
	vector< ServiceListener< OrderBook<Bond> >* > listenersCollection;
};

/**
* market data Connector to flow trades into Bond Booking Service.
* Subscribe only
*/
class BondMarketDataConnector : public Connector< OrderBook<Bond> >
{
public:

	//ctor
	BondMarketDataConnector(BondMarketDataService& targetService) : d_service(targetService) {
	};

	//subscirbe method to flow the data into service
	void ReadFromSource(string SourceName);

	void Publish(OrderBook<Bond> &data) {}

private:
	BondMarketDataService& d_service;

};

//*********************************************************************************************
// implmentation part for Order class:

Order::Order(double _price, long _quantity, PricingSide _side)
{
  price = _price;
  quantity = _quantity;
  side = _side;
}

double Order::GetPrice() const
{
  return price;
}
 
long Order::GetQuantity() const
{
  return quantity;
}
 
PricingSide Order::GetSide() const
{
  return side;
}
//*********************************************************************************************

//*********************************************************************************************
// implmentation part for BidOffer class:
BidOffer::BidOffer(const Order &_bidOrder, const Order &_offerOrder) :
  bidOrder(_bidOrder), offerOrder(_offerOrder)
{
}

const Order& BidOffer::GetBidOrder() const
{
  return bidOrder;
}

const Order& BidOffer::GetOfferOrder() const
{
  return offerOrder;
}

//*********************************************************************************************
// implmentation part for orderbook class:
template<typename T>
OrderBook<T>::OrderBook(const T &_product, const vector<Order> &_bidStack, const vector<Order> &_offerStack) :
  product(_product), bidStack(_bidStack), offerStack(_offerStack)
{
}

template<typename T>
const T& OrderBook<T>::GetProduct() const
{
  return product;
}

template<typename T>
const vector<Order>& OrderBook<T>::GetBidStack() const
{
  return bidStack;
}

template<typename T>
const vector<Order>& OrderBook<T>::GetOfferStack() const
{
  return offerStack;
}
//*********************************************************************************************


//*********************************************************************************************
// implmentation part for orderbook class:


// Get data on our service given a key
OrderBook<Bond>& BondMarketDataService::GetData(string cusip_num)
{
	for (auto ele : orderbooksCollection)
	{
		if (cusip_num == ele.GetProduct().GetProductId())
			return ele;
	}
	exit(-1);
}

// The callback that a Connector should invoke for any new or updated data
void BondMarketDataService::OnMessage(OrderBook<Bond>& order_book)
{
	orderbooksCollection.push_back(order_book);
	for (auto ele : listenersCollection)
		ele->ProcessAdd(order_book);
}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondMarketDataService::AddListener(ServiceListener< OrderBook<Bond> >* listener)
{
	listenersCollection.push_back(listener);
}

// Get all liysteners on the Service.
const vector< ServiceListener< OrderBook<Bond> >* >& BondMarketDataService::GetListeners() const
{
	return listenersCollection;
}

// Get the best bid/offer order
const BidOffer& BondMarketDataService::GetBestBidOffer(const string &cusip_num)
{
	double bestBid = -2, bestOffer = 10000000000;
	bool find = false;
	for (auto ele : orderbooksCollection)
	{
		if (ele.GetProduct().GetProductId() == cusip_num)
		{
			find = true;
			bestBid = ele.GetBidStack()[0].GetPrice() > bestBid ? ele.GetBidStack()[0].GetPrice() : bestBid;
			bestOffer = ele.GetOfferStack()[0].GetPrice() < bestOffer ? ele.GetOfferStack()[0].GetPrice() : bestOffer;

		}
	}
	if (find)
	{
		return BidOffer(Order(bestBid, 10000000, BID), Order(bestOffer, 10000000, OFFER));
	}
	else
	{
		cout << "No result being found." << endl;
		exit(-1);
	}

}


//*********************************************************************************************

//*********************************************************************************************
// implmentation BondMarketDataConnector part for  class:

void BondMarketDataConnector::ReadFromSource(string SourceName)
{
	fstream source(SourceName);
	string delimiter = ",";
	string delimiter2 = "-";

	int pos = 0;
	string cur_line;
	getline(source, cur_line);
	while (source)
	{
		
		int pos = 0;

		//parse the cusip number
		pos = cur_line.find(delimiter);
		string cusip = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		//parse the integer part of the price
		pos = cur_line.find(delimiter2);
		string price_integer = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		//parse the digit part]
		string price_first_two = cur_line.substr(0, 2);
		string price_third = cur_line.substr(2, 1);

		vector<Order> bid_stack;
		vector<Order> offer_stack;
		//convert price string into numerical and store it
		double mid_price;
		if (price_third == "+")
			mid_price = stod(price_integer) + stod(price_first_two) / 32.0 + 4.0 / 256.0;
		else
			mid_price = stod(price_integer) + stod(price_first_two) / 32.0 + stod(price_third) / 256.0;
		
		double spread = 1.0 / 256.0;
		for (int i = 1; i <= 5; i++)
		{
			Order bid(mid_price + spread*i, i * 10000000, BID);
			bid_stack.push_back(bid);
			
			Order offer(mid_price - spread*i, i * 10000000, BID);
			offer_stack.push_back(offer);
		}
		
		//cout << mid_price << endl;
		OrderBook<Bond> temp_order_book(Bond(cusip), bid_stack, offer_stack);
		d_service.OnMessage(temp_order_book);

		getline(source, cur_line);
		

	}

	source.close();



		
}
#endif
