#ifndef MARKETDATALISTENER_HPP
#define MARKETDATALISTENER_HPP

#include "marketdataservice.hpp"
#include "BondAlgoExecutionService.hpp"

using namespace std;

class MarketDataServiceListener : public ServiceListener< OrderBook<Bond> >
{
public:
	MarketDataServiceListener(BondAlgoExecutionService &_algoExecutionService) : d_service(_algoExecutionService) {}
	void ProcessAdd(OrderBook<Bond> &orderbook);
	void ProcessRemove(OrderBook<Bond> &orderbook) {}
	void ProcessUpdate(OrderBook<Bond> &orderbook) {}
private:
	BondAlgoExecutionService &d_service;
};

void MarketDataServiceListener::ProcessAdd(OrderBook<Bond> &orderbook)
{
	d_service.OnMessage(orderbook);
}


#endif
