
#ifndef POSITIONLISTEN_HPP
#define POSITIONLISTEN_HPP
#include "historicaldataservice.hpp"

using namespace std;

class PositionListener : public ServiceListener< Position<Bond> >
{
public:
  //ctor
  PositionListener(BondHistoricalPositionDataService &_historicalPositionService) : d_service(_historicalPositionService){}
  
  void ProcessAdd(Position<Bond> &position);
  
  void ProcessRemove(Position<Bond> &position) {}
  
  void ProcessUpdate(Position<Bond> &position) {}
  
private:
  BondHistoricalPositionDataService & d_service;
  
};

void PositionListener::ProcessAdd(Position<Bond> &position)
{
  d_service.OnMessage(position);
}

#endif