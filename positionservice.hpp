/**
 * positionservice.hpp
 * Defines the data types and Service for positions.
 *
 * @author Breman Thuraisingham
 */
#ifndef POSITION_SERVICE_HPP
#define POSITION_SERVICE_HPP

#include <string>
#include <map>
#include <vector>
#include "soa.hpp"
#include "tradebookingservice.hpp"

using namespace std;

/**
 * Position class in a particular book.
 * Type T is the product type.
 */
template<typename T>
class Position
{

public:

  // ctor for a position
  Position(const T &_product);
  Position(Trade<T> trade);
  Position() {}


  // Get the product
  const T& GetProduct() const;

  // Get the position quantity
  long GetPosition(string &book);

  // Get the aggregate position
  long GetAggregatePosition();

  //Parse given trade to the positions
  void ParseTrade(Trade<T> trade);

private:
  T product;
  map<string,long> positions;

};

/**
 * Position Service to manage positions across multiple books and secruties.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class PositionService : public Service<string,Position <T> >
{

public:

  // Add a trade to the service
  virtual void AddTrade(const Trade<T> &trade) = 0;

};

class BondPositionService : public PositionService<Bond>
{

public:
	

	// Get data on our service given a key
	Position<Bond>& GetData(string cusip_num);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(Position<Bond> &position);


	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener< Position<Bond> >* listener);

	// Get all listeners on the Service.
	const vector< ServiceListener< Position<Bond> >* >& GetListeners() const;
	
	// Add a trade to the service
	void AddTrade(const Trade<Bond> &trade);
	
private:
	map< string, Position<Bond> > positionMapping;
	vector< ServiceListener< Position<Bond> >* > listenersCollection;
};



//*********************************************************************************************
// implmentation part for Position class:

template<typename T>
Position<T>::Position(const T &_product) :
  product(_product)
{
}

template<typename T>
Position<T>::Position(Trade<T> trade): product(trade.GetProduct())
{
	if (trade.GetSide() == BUY)
		positions[trade.GetBook()] += trade.GetQuantity();
	else
		positions[trade.GetBook()] -= trade.GetQuantity();
}

template<typename T>
const T& Position<T>::GetProduct() const
{
  return product;
}

template<typename T>
long Position<T>::GetPosition(string &book)
{
  return positions[book];
}

template<typename T>
long Position<T>::GetAggregatePosition()
{
	//sum up all the positions
	long res = 0;
	for (auto ele : positions) {
		res += ele.second;
	}
	return res;
}

template<typename T>
void Position<T>::ParseTrade(Trade<T> trade)
{
	auto search_res = positions.find(trade.GetBook());
	if (search_res != positions.end())
		positions[trade.GetBook()] += trade.GetSide() == BUY ? trade.GetQuantity() : -trade.GetQuantity();
	else //do not find the book
		positions.insert(make_pair(trade.GetBook(), trade.GetSide() == BUY ? trade.GetQuantity() : -trade.GetQuantity()));
}

//*********************************************************************************************

//*********************************************************************************************
// implmentation part for BondPositionService class:


// Get data on our service given a key
Position<Bond>& BondPositionService::GetData(string cusip_num)
{
	return positionMapping[cusip_num];
}

// The callback that a Connector should invoke for any new or updated data
void BondPositionService::OnMessage(Position<Bond> &position)
{
	for (auto listener : listenersCollection)
	{
		listener->ProcessAdd(position);
	}
}


// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondPositionService::AddListener(ServiceListener< Position<Bond> >* listener)
{
	listenersCollection.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener< Position<Bond> >* >& BondPositionService::GetListeners() const
{
	return listenersCollection;
}

// Add a trade to the service
void BondPositionService::AddTrade(const Trade<Bond> &trade)
{
	auto search_res = positionMapping.find(trade.GetProduct().GetProductId());
	Position<Bond> temp(trade);
	if (search_res != positionMapping.end()) //the productId is found
	{
		positionMapping[trade.GetProduct().GetProductId()].ParseTrade(trade);
		OnMessage(temp);
	}
	else // the productId was not found
	{
		positionMapping[trade.GetProduct().GetProductId()] = temp;
		OnMessage(temp);		
	}

}


//*********************************************************************************************

#endif
