#ifndef POSITIONLISTENER_HPP
#define POSITIONLISTENER_HPP


#include "historicaldataservice.hpp"
#include "riskservice.hpp"

using namespace std;

class PositionDataServiceListener : public ServiceListener< Position<Bond> >
{
public:
	//ctor
	PositionDataServiceListener(BondRiskService &_riskService) : d_service(_riskService) {}

	void ProcessAdd(Position<Bond> &position);

	void ProcessRemove(Position<Bond> &position) {}

	void ProcessUpdate(Position<Bond> &position) {}

private:
	BondRiskService &d_service;

};

void PositionDataServiceListener::ProcessAdd(Position<Bond> &position)
{
	d_service.AddPosition(position);
}

#endif