/**
 * pricingservice.hpp
 * Defines the data types and Service for internal prices.
 *
 * @author Breman Thuraisingham
 */
#ifndef PRICING_SERVICE_HPP
#define PRICING_SERVICE_HPP

#include <string>
#include <fstream>
#include "soa.hpp"
#include "products.hpp"


/**
 * A price object consisting of mid and bid/offer spread.
 * Type T is the product type.
 */
template<typename T>
class Price
{

public:

  // ctor for a price
  Price(const T &_product, double _mid, double _bidOfferSpread);

  // Get the product
  const T& GetProduct() const;

  // Get the mid price
  double GetMid() const;

  // Get the bid/offer spread around the mid
  double GetBidOfferSpread() const;

private:
  const T& product;
  double mid;
  double bidOfferSpread;

};

/**
 * Pricing Service managing mid prices and bid/offers.
 * Keyed on product identifier.
 * Type T is the product type.
 */

template<typename T>
class PricingService : public Service<string, Price <T> >
{
};




class BondPricingService : public PricingService<Bond> 
{

public:
  // Get data on our service given a key
  Price<Bond>& GetData(string cusip_num);

  // The callback that a Connector should invoke for any new or updated data
  void OnMessage(Price<Bond> &price);


  // Add a listener to the Service for callbacks on add, remove, and update events
  // for data to the Service.
  void AddListener(ServiceListener< Price<Bond> >* listener);

  // Get all listeners on the Service.
  const vector< ServiceListener< Price<Bond> >* >& GetListeners() const;
  



private:
  //vector store trade data
  vector< Price<Bond> > pricesCollection;

  //vector store all the listener
  vector< ServiceListener< Price<Bond> >* > listenersCollection;

};


/**
 * Bond Pricing Connector to flow trades into Bond Booking Service.
 * Subscribe only
 */

class BondPricingConnector : public Connector< Price<Bond> >
{
public:

  //ctor
  BondPricingConnector (BondPricingService& targetService): d_service(targetService){
  };

  //subscirbe method to flow the data into service
  void ReadFromSource(string SourceName);

  void Publish(Price<Bond> &data) {}

private:
  BondPricingService& d_service;
  
};


//*********************************************************************************************
// implmentation part for Price class:


template<typename T>
Price<T>::Price(const T &_product, double _mid, double _bidOfferSpread) :
  product(_product)
{
  mid = _mid;
  bidOfferSpread = _bidOfferSpread;
}

template<typename T>
const T& Price<T>::GetProduct() const
{
  return product;
}

template<typename T>
double Price<T>::GetMid() const
{
  return mid;
}

template<typename T>
double Price<T>::GetBidOfferSpread() const
{
  return bidOfferSpread;
}

//*********************************************************************************************

//*********************************************************************************************
//implementation part for BondPricingService class:

Price<Bond>& BondPricingService::GetData(string cusip_num)
{

  //iterate through data we have and return the data if we got the corresponding productId
  for(auto price: pricesCollection)
  {
    if (price.GetProduct().GetProductId() == cusip_num )
    {
      return price;
    }
  }

}

void BondPricingService::OnMessage(Price<Bond> &price)
{
  

  //add event to service happend call the ProcessAdd method for each listener
	for (auto listener : listenersCollection)
	{
		listener->ProcessAdd(price);
	}

  pricesCollection.push_back(price);
}

void BondPricingService::AddListener(ServiceListener< Price<Bond> >* listener)
{
	listenersCollection.push_back(listener);
}

const vector< ServiceListener< Price<Bond> >* >& BondPricingService::GetListeners() const
{
	return listenersCollection;
}

//*********************************************************************************************

//*********************************************************************************************
//implementation part for BondPricingConnector class:

void BondPricingConnector::ReadFromSource(string SourceName) {

	fstream source(SourceName);
	string delimiter = ",";
	string delimiter2 = "-";

	int pos = 0;
	string cur_line;
	getline(source, cur_line);
	while (source)
	{
		
		int pos = 0;

		//parse the cusip number
		pos = cur_line.find(delimiter);
		string cusip = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		//parse the mid-price
		pos = cur_line.find(delimiter);
		string mid_price_str = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());


		//parse the spread
		pos = cur_line.find(delimiter);
		string spread_str = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		//convert the mid_price into numerical number
		pos = mid_price_str.find(delimiter2);
		string mid_price_integer = mid_price_str.substr(0, pos);
		mid_price_str.erase(0, pos + delimiter.length());

		//parse the digit part
		string mid_price_first_two = mid_price_str.substr(0, 2);
		string mid_price_third = mid_price_str.substr(2, 1);
		//convert the spread into numerical number
		pos = spread_str.find(delimiter2);
		string spread_integer = spread_str.substr(0, pos);
		spread_str.erase(0, pos + delimiter.length());

		//parse the digit part
		string spread_first_two = spread_str.substr(0, 2);
		string spread_third = spread_str.substr(2, 1);

		double mid_price;
		if (mid_price_third == "+")
			mid_price = stod(mid_price_integer) + stod(mid_price_first_two) / 32.0 + 4.0 / 256.0;
		else
			mid_price = stod(mid_price_integer) + stod(mid_price_first_two) / 32.0 + stod(mid_price_third) / 256.0;

		double spread;
		if (spread_third == "+")
			spread = stod(spread_integer) + stod(spread_first_two) / 32.0 + 4.0 / 256.0;
		else
			spread = stod(spread_integer) + stod(spread_first_two) / 32.0 + stod(spread_third) / 256.0;

		Price<Bond> tempPrice(Bond(cusip), mid_price, spread);

		d_service.OnMessage(tempPrice);

		getline(source, cur_line);

	}

	source.close();
	

}

#endif
