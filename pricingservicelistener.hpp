#ifndef PRCINGSERVICELISTENER_HPP
#define PRCINGSERVICELISTENER_HPP

#include "BondAlgoStreamingService.hpp"

using namespace std;

class PricingServiceListener : public ServiceListener< Price<Bond> >
{
public:
	//ctor
	PricingServiceListener(BondAlgoStreamingService & _algoStreamingService) : d_service(_algoStreamingService) {}

	void ProcessAdd(Price<Bond> &price);

	void ProcessRemove(Price<Bond> &data) {}

	void ProcessUpdate(Price<Bond> &data) {}

private:
	BondAlgoStreamingService & d_service;

};

void PricingServiceListener::ProcessAdd(Price<Bond>& price)
{
	d_service.OnMessage(price);
}
#endif
