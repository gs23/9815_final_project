/**
 * riskservice.hpp
 * Defines the data types and Service for fixed income risk.
 *
 * @author Breman Thuraisingham
 */
#ifndef RISK_SERVICE_HPP
#define RISK_SERVICE_HPP

#include <vector>
#include <iostream>
#include <string>
#include <map>
#include "soa.hpp"
#include "positionservice.hpp"

/**
 * PV01 risk.
 * Type T is the product type.
 */

using namespace std;

double NaiveBondPV01(string cusip);

template<typename T>
class PV01
{

public:

  // ctor for a PV01 value
  PV01(const vector<T> &_products, double _pv01, long _quantity);

  // Get the products associated with this PV01 value
  const vector<T>& GetProducts() const;

  // Get the PV01 value
  double GetPV01() const;

  // Get the quantity that this risk value is associated with
  long GetQuantity() const;

  void SetQuantity(long _quantity) { quantity = _quantity; }

private:
  vector<T> products;
  double pv01;
  long quantity;

};

/**
 * A bucket sector to bucket a group of securities.
 * We can then aggregate bucketed risk to this bucket.
 * Type T is the product type.
 */
template<typename T>
class BucketedSector
{

public:

  // ctor for a bucket sector
  BucketedSector(const vector<T> &_products, string _name);

  // Get the products associated with this bucket
  const vector<T>& GetProducts() const;

  // Get the name of the bucket
  const string& GetName() const;

private:
  vector<T> products;
  string name;

};

/**
 * Risk Service to vend out risk for a particular security and across a risk bucketed sector.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class RiskService : public Service<string,PV01 <T> >
{

public:

  // Add a position that the service will risk
  virtual void AddPosition(Position<T> &position) = 0;

  // Get the bucketed risk for the bucket sector
  virtual const PV01<T>& GetBucketedRisk(const BucketedSector<T> &sector) const = 0;

};

class BondRiskService : public RiskService<Bond>
{
public:

	// Get data on our service given a key
	PV01<Bond>& GetData(string cusip_num);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(PV01<Bond> &position);


	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener< PV01<Bond> >* listener);

	// Get all listeners on the Service.
	const vector< ServiceListener< PV01<Bond> >* >& GetListeners() const ;
	
	// Add a position that the service will risk
	void AddPosition(Position<Bond> &position);

	// Get the bucketed risk for the bucket sector
	const PV01<Bond>& GetBucketedRisk(const BucketedSector<Bond> &sector) const;

	void AddHistoricalDataListener(ServiceListener< map<string, PV01<Bond> > >* listener);

private:
	vector < ServiceListener< PV01<Bond> >* > listenersCollection;
	vector < ServiceListener< map<string, PV01<Bond> > >*> historicalDataListenersCollection;
	map< string, PV01<Bond> > pv01Mapping;
};


//*********************************************************************************************
// implmentation part for PV01 class:


template<typename T>
PV01<T>::PV01(const vector<T> &_products, double _pv01, long _quantity) :
  products(_products)
{
  pv01 = _pv01;
  quantity = _quantity;
}

template<typename T>
const vector<T>& PV01<T>::GetProducts() const
{
  return products;
}

template<typename T>
double PV01<T>::GetPV01() const
{
  return pv01;
}

template<typename T>
long PV01<T>::GetQuantity() const
{
  return quantity;
}

//*********************************************************************************************

//*********************************************************************************************
// implmentation part for BucketdSector class:

template<typename T>
BucketedSector<T>::BucketedSector(const vector<T>& _products, string _name) :
  products(_products)
{
  name = _name;
}

template<typename T>
const vector<T>& BucketedSector<T>::GetProducts() const
{
  return products;
}

template<typename T>
const string& BucketedSector<T>::GetName() const
{
  return name;
}

//*********************************************************************************************

//*********************************************************************************************
// implmentation part for BondRiskService class:

// Get data on our service given a key
PV01<Bond>& BondRiskService::GetData(string cusip_num)
{
	auto iter = pv01Mapping.find(cusip_num);
	if (iter != pv01Mapping.end())
	{
		return iter->second;
	}
	else
	{
		cout << "Not Found!" << endl;
		exit(-1);
	}
}

// The callback that a Connector should invoke for any new or updated data
void BondRiskService::OnMessage(PV01<Bond> &position)
{

}


// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondRiskService::AddListener(ServiceListener< PV01<Bond> >* listener)
{
	listenersCollection.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener< PV01<Bond> >* >& BondRiskService::GetListeners() const
{
	return listenersCollection;
}

// Add a position that the service will risk
void BondRiskService::AddPosition(Position<Bond> &position)
{
	string cusip_num = position.GetProduct().GetProductId();
	auto iter = pv01Mapping.find(cusip_num);

	if (iter != pv01Mapping.end())
	{
		long quantity = iter->second.GetQuantity()+position.GetAggregatePosition();
		iter->second.SetQuantity(quantity);
	}
	else
	{
		vector<Bond> PVvec{ position.GetProduct() };
		PV01<Bond> temp(PVvec, NaiveBondPV01(position.GetProduct().GetProductId()), position.GetAggregatePosition());
		pv01Mapping.insert(make_pair(position.GetProduct().GetProductId(), temp));
	}

	for (auto listener : historicalDataListenersCollection)
		listener->ProcessAdd(pv01Mapping);

}

// Get the bucketed risk for the bucket sector
const PV01<Bond>& BondRiskService::GetBucketedRisk(const BucketedSector<Bond> &sector) const
{
	auto bucket = sector.GetProducts();
	double res = 0;
	long quantity = 0;
	for (auto bond : bucket)
	{
		auto iter = pv01Mapping.find(bond.GetProductId());
		if (iter != pv01Mapping.end())
		{
			//aggregate pv01
			res += NaiveBondPV01(bond.GetProductId())*iter->second.GetQuantity();
			quantity += iter->second.GetQuantity();
		}
		
	}
	return PV01<Bond>(sector.GetProducts(), res, quantity);
}

void BondRiskService::AddHistoricalDataListener(ServiceListener< map<string, PV01<Bond> > > *listener)
{
	historicalDataListenersCollection.push_back(listener);
}


//*********************************************************************************************


double NaiveBondPV01(string cusip)
{

	if (cusip == "912828M12") return 0.11824568;
	else if (cusip == "912828R37") return 0.19527589;
	else if (cusip == "912828N68") return 0.047720509;
	else if (cusip == "912828M59") return 0.09405992;
	else if (cusip == "912828M46") return 0.141998751;
	else if (cusip == "912828R43") return 0.040704295;
	return 0;

}
#endif
