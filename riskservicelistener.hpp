#ifndef RISKLISTENER_HPP
#define RISKLISTENER_HPP

#include "historicaldataservice.hpp"
#include "riskservice.hpp"

using namespace std;

class RiskServiceListener : public ServiceListener< map<string, PV01<Bond> >  >
{
public:
	//ctor
	RiskServiceListener(BondHistoricalRiskDataService &_historicalRiskService) : d_service(_historicalRiskService) {}

	void ProcessAdd(map<string, PV01<Bond> > &pv01);

	void ProcessRemove(map<string, PV01<Bond> > &pv01) {}

	void ProcessUpdate(map<string, PV01<Bond> > &pv01) {}

private:
	BondHistoricalRiskDataService &d_service;

};

void RiskServiceListener::ProcessAdd(map<string, PV01<Bond> > &pv01)
{
	d_service.OnMessage(pv01);
}
#endif