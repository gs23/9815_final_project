/**
 * streamingservice.hpp
 * Defines the data types and Service for price streams.
 *
 * @author Breman Thuraisingham
 */
#ifndef STREAMING_SERVICE_HPP
#define STREAMING_SERVICE_HPP

#include "soa.hpp"
#include "marketdataservice.hpp"
#include <map>

/**
 * A price stream order with price and quantity (visible and hidden)
 */
class PriceStreamOrder
{

public:

  // ctor for an order
  PriceStreamOrder(double _price, long _visibleQuantity, long _hiddenQuantity, PricingSide _side);

  // The side on this order
  PricingSide GetSide() const;

  // Get the price on this order
  double GetPrice() const;

  // Get the visible quantity on this order
  long GetVisibleQuantity() const;

  // Get the hidden quantity on this order
  long GetHiddenQuantity() const;

private:
  double price;
  long visibleQuantity;
  long hiddenQuantity;
  PricingSide side;

};

/**
 * Price Stream with a two-way market.
 * Type T is the product type.
 */
template<typename T>
class PriceStream
{

public:

  // ctor
  PriceStream(const T &_product, const PriceStreamOrder &_bidOrder, const PriceStreamOrder &_offerOrder);

  // Get the product
  const T& GetProduct() const;

  // Get the bid order
  const PriceStreamOrder& GetBidOrder() const;

  // Get the offer order
  const PriceStreamOrder& GetOfferOrder() const;

private:
  T product;
  PriceStreamOrder bidOrder;
  PriceStreamOrder offerOrder;

};

/**
 * Streaming service to publish two-way prices.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class StreamingService : public Service<string,PriceStream <T> >
{

public:

  // Publish two-way prices
  virtual void PublishPrice(PriceStream<T> priceStream) = 0;

};


class BondStreamingService : public StreamingService<Bond>
{

public:
	PriceStream<Bond>& GetData(string cusip_num);

	void OnMessage(PriceStream<Bond> &priceStream);

	void AddListener(ServiceListener< PriceStream<Bond> >* listener);

	const vector< ServiceListener< PriceStream<Bond> >* >& GetListeners() const ;

	void PublishPrice(PriceStream<Bond> priceStream);

private:
	map<string, PriceStream<Bond> > priceStreamMapping;

	vector< ServiceListener< PriceStream<Bond> >* > listenersCollection;
};



//*********************************************************************************************
// implmentation part for PriceStreamOrder class:

PriceStreamOrder::PriceStreamOrder(double _price, long _visibleQuantity, long _hiddenQuantity, PricingSide _side)
{
  price = _price;
  visibleQuantity = _visibleQuantity;
  hiddenQuantity = _hiddenQuantity;
  side = _side;
}

double PriceStreamOrder::GetPrice() const
{
  return price;
}

long PriceStreamOrder::GetVisibleQuantity() const
{
  return visibleQuantity;
}

long PriceStreamOrder::GetHiddenQuantity() const
{
  return hiddenQuantity;
}

template<typename T>
PriceStream<T>::PriceStream(const T &_product, const PriceStreamOrder &_bidOrder, const PriceStreamOrder &_offerOrder) :
  product(_product), bidOrder(_bidOrder), offerOrder(_offerOrder)
{
}

template<typename T>
const T& PriceStream<T>::GetProduct() const
{
  return product;
}

template<typename T>
const PriceStreamOrder& PriceStream<T>::GetBidOrder() const
{
  return bidOrder;
}

template<typename T>
const PriceStreamOrder& PriceStream<T>::GetOfferOrder() const
{
  return offerOrder;
}
//*********************************************************************************************


PriceStream<Bond>& BondStreamingService::GetData(string cusip_num)
{
	auto iter = priceStreamMapping.find(cusip_num);
	return iter->second;
}

void BondStreamingService::OnMessage(PriceStream<Bond>& priceStream)
{
	PublishPrice(priceStream);
}


void BondStreamingService::AddListener(ServiceListener<PriceStream<Bond>>* listener)
{
	listenersCollection.push_back(listener);
}


const vector<ServiceListener<PriceStream<Bond>>*>& BondStreamingService::GetListeners() const
{
	return listenersCollection;
}

void BondStreamingService::PublishPrice(PriceStream<Bond> priceStream)
{
	priceStreamMapping.insert(make_pair(priceStream.GetProduct().GetProductId(), priceStream));
	for (auto listener : listenersCollection)
		listener->ProcessAdd(priceStream);
}
#endif
