#ifndef STREAMINGLISTENER_HPP
#define STREAMINGLISTENER_HPP
#include "historicaldataservice.hpp"

using namespace std;

class StreamingServiceListener : public ServiceListener< PriceStream<Bond> >
{
public:
	//ctor
	StreamingServiceListener(BondHistoricalStreamingDataService &_historicalStreamService) : d_service(_historicalStreamService) {}

	void ProcessAdd(PriceStream<Bond> &price_stream);

	void ProcessRemove(PriceStream<Bond> &price_stream) {}

	void ProcessUpdate(PriceStream<Bond> &price_stream) {}

private:
	BondHistoricalStreamingDataService &d_service;

};

void StreamingServiceListener::ProcessAdd(PriceStream<Bond> &price_stream)
{
	d_service.OnMessage(price_stream);
}


#endif