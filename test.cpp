#include <iostream>
#include "marketdataservice.hpp"
#include "tradebookingservice.hpp"
#include "pricingservice.hpp"
#include "inquiryservice.hpp"
#include "executionservice.hpp"
#include "streamingservice.hpp"
#include "BondAlgoExecutionService.hpp"
#include "positionservice.hpp"
#include "BondAlgoStreamingService.hpp"
#include "riskservice.hpp"
#include "soa.hpp"
#include "products.hpp"
#include "tradebookingservicelistener.hpp"
#include "BondAlgoExecutionService.hpp"
#include "marketdataservicelistener.hpp"
#include "pricingservicelistener.hpp"
#include "historicaldataservice.hpp"
#include "positionservicelistener.hpp"
#include "executionservicelistener.hpp"
#include "streamingservicelistener.hpp"
#include "inquiryservicelistener.hpp"
#include "riskservicelistener.hpp"
#include "positionlistener.hpp"

using namespace std;

int main(){
	
	
	/**************************************************
	* Service Initialization  and Listener Registration*
	***************************************************/


	//booking service 
	BondTradeBookingService bookingService;
	BondTradeBookingConnector bookingConnector(bookingService);

	//osition service 
	BondPositionService positionService;
	TradeBookingServiceListener tradeListener(positionService);
	bookingService.AddListener(&tradeListener);

	//pricing service
	BondPricingService pricingService;
	BondPricingConnector pricingConnector(pricingService);

	//BondRiskService
	BondRiskService riskService;
	PositionDataServiceListener positionDataServiceListener(riskService);
	positionService.AddListener(&positionDataServiceListener);
	
	//BondHistoricalRiskDataService
	BondHistoricalRiskDataConnector historicalRiskConnector;
	BondHistoricalRiskDataService historicalRiskService(historicalRiskConnector);
	
	//listener to BondRiskService
	RiskServiceListener riskServiceListener(historicalRiskService);
	riskService.AddHistoricalDataListener(&riskServiceListener);
	
	//marketDataService
	BondMarketDataService marketDataService;
	BondMarketDataConnector marketDataConnector(marketDataService);
	
	//ExecutionService
	BondExecutionService executionService;
	BondAlgoExecutionService algoExecutionService(executionService);
	
	//market data service listener
	MarketDataServiceListener  mktDataServiceListener(algoExecutionService);
	marketDataService.AddListener(&mktDataServiceListener);

	//streaming service
	BondStreamingService streamingService;
	BondAlgoStreamingService algoStreamingService(streamingService);

	//pricing service
	PricingServiceListener priceListener(algoStreamingService);
	pricingService.AddListener(&priceListener);
	
	//inquiry service
	BondInquiryService inquiryService;
	BondInquiryConnector inquiryConnector(inquiryService);
	

	//*************************************************
	//historical position service
	BondHistoricalPositionDataConnector historicalPositionConnector;
	BondHistoricalPositionDataService historicalPositionService(historicalPositionConnector);
	


	//listener to BondPositionService	
    PositionListener positionListener(historicalPositionService);
	positionService.AddListener(&positionListener);
	//******************************************


	
	//BondHistoricalExecutionDataService
	BondHistoricalExecutionDataConnector executionHistoricalConnector;
	BondHistoricalExecutionDataService executionHistoricalService(executionHistoricalConnector);
	
	
	//listener to BondExecutionService
	ExecutionServiceListener executionServiceListener(executionHistoricalService);
	executionService.AddListener(&executionServiceListener);


	
	//BondHistoricalStreamingDataService
	BondHistoricalStreamingDataConnector historicalStreamingDataConnector;
	BondHistoricalStreamingDataService historicalStreamingDataService(historicalStreamingDataConnector);
	

	//listener to BondStreamingService
	StreamingServiceListener streamingServiceListener(historicalStreamingDataService);
	streamingService.AddListener(&streamingServiceListener);

	
	
	//BondHistoricalInquiryDataService
	BondHistoricalInquiryDataConnector historicalInquiryDataConnector;
	BondHistoricalInquiryDataService historicalInquiryDataService(historicalInquiryDataConnector);
	
	//listener to BondInquiryService
	InquiryServiceListener inquiryServiceListener(historicalInquiryDataService);
	inquiryService.AddListener(&inquiryServiceListener);
	

	/*************************
	* Connector flows in data*
	**************************/
	bookingConnector.ReadFromSource("trades.txt");
	pricingConnector.ReadFromSource("prices.txt");
	marketDataConnector.ReadFromSource("marketdata.txt");
	inquiryConnector.ReadFromSource("inquiry.txt");


}