/**
 * tradebookingservice.hpp
 * Defines the data types and Service for trade booking.
 *
 * @author Breman Thuraisingham
 */
#ifndef TRADE_BOOKING_SERVICE_HPP
#define TRADE_BOOKING_SERVICE_HPP

#include <string>
#include <vector>
#include <fstream>
#include "products.hpp"
#include "soa.hpp"

// Trade sides
enum Side { BUY, SELL };

/**
 * Trade object with a price, side, and quantity on a particular book.
 * Type T is the product type.
 */
template<typename T>
class Trade
{

public:

  // ctor for a trade
  Trade(const T &_product, string _tradeId, string _book, long _quantity, Side _side);

  // Get the product
  const T& GetProduct() const;

  // Get the trade ID
  const string& GetTradeId() const;

  // Get the book
  const string& GetBook() const;

  // Get the quantity
  long GetQuantity() const;

  // Get the side
  Side GetSide() const;

private:
  T product;
  string tradeId;
  string book;
  long quantity;
  Side side;

};



/**
 * Trade Booking Service to book trades to a particular book.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class TradeBookingService : public Service<string,Trade <T> >
{

public:

  // Book the trade
  virtual void BookTrade(const Trade<T> &trade) = 0;

};

class BondTradeBookingService : public TradeBookingService<Bond>
{

public:
  
  //ctor
  BondTradeBookingService(){}

  // Get data on our service given a key
  Trade<Bond>& GetData(string tradeId);

  // The callback that a Connector should invoke for any new or updated data
  void OnMessage(Trade<Bond> &trade);


  // Add a listener to the Service for callbacks on add, remove, and update events
  // for data to the Service.
  void AddListener(ServiceListener< Trade<Bond> >* listener);

  // Get all listeners on the Service.
  const vector< ServiceListener< Trade<Bond> >* >& GetListeners() const;
  
  // Book the trade
  void BookTrade(const Trade<Bond> &trade);

private:

  //vector store trade data
  vector< Trade<Bond> > tradesCollection;

  //vector store all the listener
  vector< ServiceListener< Trade<Bond> >* > listenersCollection;
};



/**
 * Trade Booking Connector to flow trades into Bond Booking Service.
 * Subscribe only
 */

class BondTradeBookingConnector : public Connector< Trade<Bond> >
{
public:

  //ctor
  BondTradeBookingConnector (BondTradeBookingService& targetService): d_service(targetService){
  };

  //subscirbe method to flow the data into service
  void ReadFromSource(string SourceName);

  void Publish(Trade<Bond> &data) {}
private:
  BondTradeBookingService& d_service;
  
};





//*********************************************************************************************
// implmentation part for Trade class:

template<typename T>
Trade<T>::Trade(const T &_product, string _tradeId, string _book, long _quantity, Side _side) :
  product(_product)
{
  tradeId = _tradeId;
  book = _book;
  quantity = _quantity;
  side = _side;
}


template<typename T>
const T& Trade<T>::GetProduct() const
{
  return product;
}

template<typename T>
const string& Trade<T>::GetTradeId() const
{
  return tradeId;
}

template<typename T>
const string& Trade<T>::GetBook() const
{
  return book;
}

template<typename T>
long Trade<T>::GetQuantity() const
{
  return quantity;
}

template<typename T>
Side Trade<T>::GetSide() const
{
  return side;
}


//*********************************************************************************************



//*********************************************************************************************
//implementation part for BondTradeBookingConnector class:


Trade<Bond>& BondTradeBookingService::GetData(string tradeId)
{ 
  //iterate through data we have and return the data if we got the right tradeId
  for(auto iter = tradesCollection.begin(); iter != tradesCollection.end(); iter++)
  {
    if( (*iter).GetTradeId() == tradeId ) 
      return (*iter);
  }
}



void BondTradeBookingService::OnMessage(Trade<Bond> &trade)
{

  //add event to service happend call the ProcessAdd method for each listener
  for(auto listener: listenersCollection )
  {
    listener->ProcessAdd(trade);
  }


  //book the trade in to the service
  BookTrade(trade);

}




void BondTradeBookingService::AddListener(ServiceListener< Trade<Bond> >* listener)
{
  listenersCollection.push_back(listener);
}


const vector< ServiceListener< Trade<Bond> >* >& BondTradeBookingService::GetListeners() const
{
  return listenersCollection;
}
  

void BondTradeBookingService::BookTrade(const Trade<Bond> &trade)
{
  tradesCollection.push_back(trade);

}


//*********************************************************************************************




//*********************************************************************************************
//implementation part for BondTradeBookingConnector class:

void BondTradeBookingConnector::ReadFromSource(string SourceName)
{
	fstream source(SourceName);
	string delimiter = ",";

	int pos = 0;
	string cur_line;
	getline(source, cur_line);
	while (source)
	{
		
		int pos = 0;

		//parse the cusip number
		pos = cur_line.find(delimiter);
		string cusip = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		//parse the tradeId
		pos = cur_line.find(delimiter);
		string tradeId = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		//parse the book
		pos = cur_line.find(delimiter);
		string book = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		//parse the quantity
		pos = cur_line.find(delimiter);
		string quantity = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		//parse the side
		pos = cur_line.find(delimiter);
		string side_trade = cur_line.substr(0, pos);
		cur_line.erase(0, pos + delimiter.length());

		Side side = side_trade == "B" ? BUY : SELL;
    Trade<Bond>tempTrade(Bond(cusip), tradeId, book, stol(quantity), side);
		d_service.OnMessage(tempTrade);
		
		getline(source, cur_line);

	}

	source.close();

}


//*********************************************************************************************



#endif
