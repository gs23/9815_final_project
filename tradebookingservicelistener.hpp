#ifndef TRADELISTENER_HPP
#define TRADELISTENER_HPP

#include "soa.hpp"
#include "positionservice.hpp"

using namespace std;

//service listener link BondTradeBookingService to BondPositionService
class TradeBookingServiceListener : public ServiceListener< Trade <Bond> >
{
public:
	TradeBookingServiceListener(BondPositionService & _positionService) : d_service(_positionService) {}
	void ProcessAdd(Trade<Bond> &trade);
	void ProcessRemove(Trade<Bond> &trade) {}
	void ProcessUpdate(Trade<Bond> &trade) {}
private:
	BondPositionService & d_service;
};

void TradeBookingServiceListener::ProcessAdd(Trade<Bond> &trade)
{
	d_service.AddTrade(trade);
}
#endif
