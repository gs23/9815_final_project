#scirpts to generate trades.txt
import random
cusip_last_two = [12,68,59,37,46,43]
cusip_last_two = [str(number) for number in cusip_last_two]
cusip_character = ['M','N','M','R','M','R']
side_choice  =["B","S"]

for i in range(6):
    print "912828"+cusip_character[i]+cusip_last_two[i]

with open("trades.txt","w") as f:
    for i in range(60):
        
        trade_id = "TR"+str(i+1)
        cusip_number = "912828"+ cusip_character[i/10-1]+cusip_last_two[i/10-1]
        book = 'TRSY' + str(random.randint(1,3))
        quantity  = str(random.randint(1,9)*1000000)
        side = side_choice[random.randint(0,1)]
        record = ",".join([cusip_number,trade_id,book,quantity,side])
        f.write(record+"\n")
        